from flask import Flask , url_for , request ,render_template
from flask_bootstrap import Bootstrap
import pandas as pd

app = Flask(__name__)
# , template_folder='../templates', static_folder='../static'
Bootstrap(app)
# Material(app)

@app.route('/')
def index():
	return render_template('index.html ')

@app.route('/predict/',methods=['POST'])
def predict():
	cv = pd.read_pickle(r'data/CountVector.pickle')
	clf = pd.read_pickle(r'data/RFC_spam_ham.pickle')

	if request.method == 'POST' :
		comment = request.form['comment']
		data = [comment]
		vect = cv.transform(data).toarray()
		my_prediction = clf.predict(vect)

	return render_template('index.html',prediction = my_prediction)

@app.route('/dataset/')
def dataset():

	df = pd.read_csv('data/spamham.csv')
	return render_template('dataset.html',df=df)


if __name__ == '__main__':
	app.run(debug=True)